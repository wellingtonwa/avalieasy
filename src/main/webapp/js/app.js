'use strict';

var app = angular.module('avalieasy', ['ngRoute']);

function AvaliacaoController($scope, $http, $routeParams) {
    var cursoNome = $routeParams.cursoNome !== undefined ? $routeParams.cursoNome : '';
    if (cursoNome) {
        $scope.mostrar_pnl_form = true;
        $http.get('avaliar/' + cursoNome)
                .success(function (data, status, headers, config) {
                    $scope.curso = data;
                    $scope.respostas = {};
                    $scope.curso.questionario.questoes.forEach(function (entry) {
                        $scope.respostas[entry.id] = {
                            tipo: entry.tipoResposta,
                            obrigatoria: entry.obrigatoria
                        };
                    });
                });
    } else {
        $scope.mostrar_pnl_form = false;
    }
    $scope.submitForm = function () {
        var valido = true;
        $scope.curso.questionario.questoes.forEach(function (questao) {
            if (questao.obrigatoria
                    && ($scope.respostas[questao.id].resp === undefined || $scope.respostas[questao.id].resp === "")) {
                alert('A resposta da questão \'' + questao.enunciado + '\' é obrigatoria');
                valido = false;
            }
        });
        if (valido) {
            $scope.getJsonResposta();
            $http.post("avaliar/", $scope.respostasParticipante)
                    .success(function (data, status, headers, config) {
                        if (data.status === "200") {
                            $scope.mostrar_btn_parcial = true;
                            location.href = "#/parcial/" + $scope.curso.id;
                        }
                        if (data.status !== "200") {
                            $scope.mostrar_btn_parcial = false;
                        }
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        console.log(status);
                        console.log(headers);
                        console.log(config);
                    });
        }
    };

    $scope.getViewTipoResposta = function (tipoResposta) {
        return tipoResposta.toLowerCase() + '.html';
    };

    $scope.getJsonResposta = function () {
        $scope.respostasParticipante = [];
        $scope.curso.questionario.questoes.forEach(function (questao) {
            var auxResposta = {id: null, resposta: null, participante: null, alternativa: null, questao: questao.id};

            if (questao.tipoResposta === "RESPOSTA_LISTAMULTIPLA") {
                for (var key in $scope.respostas[questao.id].resp) {
                    $scope.respostasParticipante.push({
                        id: null, resposta: null, participante: null
                        , alternativa: key, questao: questao.id
                    });
                }
                return;
            }
            $scope.respostasParticipante.push(auxResposta);
            if (questao.tipoResposta === "RESPOSTA_DISSERTATIVA" && $scope.respostas[questao.id].resp) {
                auxResposta.resposta = $scope.respostas[questao.id].resp;
                return;
            }
            if (questao.tipoResposta === "RESPOSTA_NOTA" && $scope.respostas[questao.id].resp) {
                auxResposta.alternativa = $scope.respostas[questao.id].resp[0];
                return;
            }
            if (questao.tipoResposta === "RESPOSTA_SIMNAO") {
                auxResposta.alternativa = $scope.respostas[questao.id].resp;
                return;
            }

        });
    };
}

function ResultadoParcialController($scope, $http, $routeParams) {
    $scope.idCurso = $routeParams.idCurso;
    var idCurso = $routeParams.idCurso !== undefined ? $routeParams.idCurso : '';
    if (idCurso) {
        $scope.mostrar_pnl_form = true;
        $http.get("parcial/?idCurso=" + idCurso)
                .success(function (data, status, headers, config) {
                    $scope.resultados = data;
                });
    } else {
        $scope.mostrar_btn_form = false;
    }
    
}

app.config(function ($routeProvider) {
    $routeProvider
            .when('/', {
                templateUrl: 'avaliacao.html',
                controller: AvaliacaoController

            })
            .when('/:cursoNome', {
                templateUrl: 'avaliacao.html',
                controller: AvaliacaoController

            })
            .when('/parcial/:idCurso', {
                templateUrl: 'parciais.html',
                controller: ResultadoParcialController

            });
});