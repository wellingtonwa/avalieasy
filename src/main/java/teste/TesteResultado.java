/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package teste;

import br.com.avalieasy.dao.QuestionarioDao;
import br.com.avalieasy.util.DbConn;
import br.com.avalieasy.model.TipoResposta;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class TesteResultado {
    
    public static void main(String[] args) {
        TesteResultado teste = new TesteResultado();
        teste.questoesNota();
    }
    
    public void porcentagemSimNao() {
        String consulta = "SELECT (COUNT(res.id)*100)/(SELECT COUNT(res1.id) "
                + "						FROM ParticipanteResposta res1 "
                + "                                             JOIN res1.alternativa alt1 "
                + "						WHERE res1.questao = que"
                + "						AND res1.questao = que AND alt1.valor=0)"
                + "FROM ParticipanteResposta res"
                + " JOIN res.questao que WHERE que.id = 5";
        Query query = DbConn.getInstance().createQuery(consulta);
        Long object = new QuestionarioDao().getPorcentagemQuestaoSim(5l);
        System.out.println(object);
    }
    
    public void listaMultipla() {
        String consulta1 = "SELECT DISTINCT ques.enunciado, alt.descricao"
                + " , (SELECT COUNT(pres1.id)"
                + "         From ParticipanteResposta pres1"
                + "         WHERE pres1.questao = ques AND pres1.alternativa = alt)"
                + "     FROM Questao ques "
                + "     LEFT JOIN ques.respostas pres "
                + "     JOIN ques.alternativas alt"
                + "     WHERE ques.tipoResposta = :tipoResposta";
        
        /*String consulta = "SELECT DISTINCT ques.enunciado, alt.descricao"
                + " , (SELECT COUNT(pres1.id)"
                + "         From ParticipanteResposta pres1"
                + "         WHERE pres1.questao = ques AND pres1.alternativa = alt)"
                + "     FROM ParticipanteResposta pres "
                + "     JOIN pres.questao ques "
                + "     JOIN pres.alternativa alt"
                + "     WHERE ques.tipoResposta = :tipoResposta";*/
        Query query = DbConn.getInstance().createQuery(consulta1).setParameter("tipoResposta", TipoResposta.RESPOSTA_LISTAMULTIPLA);
        
        List<Object[][]> objects = query.getResultList();
        for(Object[] forObject : objects){
            System.out.println(forObject[0]+" "+forObject[1]+" "+forObject[2]);
        }
        System.out.println(objects.size());
    }
    
    
    
    public void questoesNota() {
        Long idCurso = 1l;
        String cst = "SELECT DISTINCT questao.enunciado, questao.tipoResposta, "
                + " (SELECT SUM(alt.valor)/COUNT(resp.id) FROM ParticipanteResposta resp"
                + " JOIN resp.alternativa alt WHERE resp.questao = questao)"
                + " FROM Curso curso "
                + " JOIN curso.questionario questionario "
                + " JOIN questionario.questoes questao "
                + " JOIN questao.alternativas alternativa "
                + " WHERE curso.id = :id "
                + " AND questao.tipoResposta = :tipoResposta";
        
        Query query = DbConn.getInstance().createQuery(cst).setParameter("id", idCurso)
                .setParameter("tipoResposta", TipoResposta.RESPOSTA_NOTA).setMaxResults(1);
        
        Object[] resultado =  (Object[]) query.getSingleResult();
        
        System.out.println(resultado[0]+" - "+resultado[1]+" - "+resultado[2]);
        
        
        String consulta ="SELECT DISTINCT questao.enunciado, questao.tipo_resposta, resp.id_alternativa, alternativa.descricao,"
  +" (SELECT COUNT(resp1.id_alternativa) FROM participante_resposta resp1 WHERE resp1.id_alternativa = resp.id_alternativa)"
 +" FROM `participante_resposta` resp"
   +" INNER JOIN questao questao ON questao.id = resp.id_questao"
   +" INNER JOIN alternativa alternativa ON alternativa.id = resp.id_alternativa"
   +" INNER JOIN questionario questionario ON questionario.id = questao.id_questionario"
   +" INNER JOIN curso curso ON curso.id = questionario.id_curso"
   +" WHERE resp.id_alternativa IS NOT NULL AND questao.tipo_resposta = 2";
        
    }
    
    
}
