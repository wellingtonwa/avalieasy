/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import br.com.avalieasy.model.Resposta_JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wellington
 */
public class TesteJackson {
    public static void main(String[] args) throws JsonProcessingException {
        String jsonRespostas = "[{\"id\":null,\"resposta\":null,\"participante\":null,\"alternativa\":\"76\",\"questao\":12},{\"id\":null,\"resposta\":null,\"participante\":null,\"alternativa\":\"67\",\"questao\":9},{\"id\":null,\"resposta\":null,\"participante\":null,\"alternativa\":\"65\",\"questao\":10},{\"id\":null,\"resposta\":null,\"participante\":null,\"alternativa\":\"58\",\"questao\":8},{\"id\":null,\"participante\":null,\"alternativa\":null,\"questao\":11}]";
        try {
//            Respostas_JSON[] respostasJson = new ObjectMapper().readValue(jsonRespostas, Respostas_JSON[].class);
            List<Resposta_JSON> respostasJson = new ObjectMapper().readValue(jsonRespostas, new TypeReference<List<Resposta_JSON>>(){});
            for(Resposta_JSON forResposta : respostasJson){
                System.out.println(forResposta.getQuestao());
            }
        } catch (IOException ex) {
            Logger.getLogger(TesteJackson.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
