/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.avalieasy.util;

import br.com.avalieasy.model.Alternativa;
import br.com.avalieasy.model.Questao;
import br.com.avalieasy.model.Questionario;
import br.com.avalieasy.model.TipoResposta;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class TemplateQuestionario {
    
    public static Questionario getQuestionario(String nomeCurso) {
//        Curso curso = new Curso();
//        curso.setNome(nomeCurso);
        Questionario questionario = new Questionario();
        questionario.setDescricao("Questinário de reação");
        questionario.setNome("Questionário "+nomeCurso);
//        curso.setQuestionario(questionario);
//        questionario.setCurso(curso);
        Questao q1 = new Questao("Como você avaliaria sua expectativa em relação ao curso?", 0, true, TipoResposta.RESPOSTA_NOTA, questionario);
        q1.getAlternativas().add(new Alternativa("1", 1, 0, q1));
        q1.getAlternativas().add(new Alternativa("2", 2, 1, q1));
        q1.getAlternativas().add(new Alternativa("3", 3, 2, q1));
        q1.getAlternativas().add(new Alternativa("4", 4, 3, q1));
        q1.getAlternativas().add(new Alternativa("5", 5, 4, q1));
        q1.getAlternativas().add(new Alternativa("6", 6, 5, q1));
        q1.getAlternativas().add(new Alternativa("7", 7, 6, q1));
        q1.getAlternativas().add(new Alternativa("8", 8, 7, q1));
        q1.getAlternativas().add(new Alternativa("9", 9, 8, q1));
        q1.getAlternativas().add(new Alternativa("10", 10, 9, q1));
        questionario.getQuestoes().add(q1);
        
        Questao q2 = new Questao("Como você avaliaria a realização do curso?", 1, true, TipoResposta.RESPOSTA_NOTA, questionario);
        q2.getAlternativas().add(new Alternativa("1", 1, 0, q2));
        q2.getAlternativas().add(new Alternativa("2", 2, 1, q2));
        q2.getAlternativas().add(new Alternativa("3", 3, 2, q2));
        q2.getAlternativas().add(new Alternativa("4", 4, 3, q2));
        q2.getAlternativas().add(new Alternativa("5", 5, 4, q2));
        q2.getAlternativas().add(new Alternativa("6", 6, 5, q2));
        q2.getAlternativas().add(new Alternativa("7", 7, 6, q2));
        q2.getAlternativas().add(new Alternativa("8", 8, 7, q2));
        q2.getAlternativas().add(new Alternativa("9", 9, 8, q2));
        q2.getAlternativas().add(new Alternativa("10", 10, 9, q2));
        questionario.getQuestoes().add(q2);
        
        Questao q3 = new Questao("Que nota você daria para o instrutor do treinamento?", 2, true, TipoResposta.RESPOSTA_NOTA, questionario);
        q3.getAlternativas().add(new Alternativa("1", 1, 0, q3));
        q3.getAlternativas().add(new Alternativa("2", 2, 1, q3));
        q3.getAlternativas().add(new Alternativa("3", 3, 2, q3));
        q3.getAlternativas().add(new Alternativa("4", 4, 3, q3));
        q3.getAlternativas().add(new Alternativa("5", 5, 4, q3));
        q3.getAlternativas().add(new Alternativa("6", 6, 5, q3));
        q3.getAlternativas().add(new Alternativa("7", 7, 6, q3));
        q3.getAlternativas().add(new Alternativa("8", 8, 7, q3));
        q3.getAlternativas().add(new Alternativa("9", 9, 8, q3));
        q3.getAlternativas().add(new Alternativa("10", 10, 9, q3));
        questionario.getQuestoes().add(q3);
        
        Questao q4 = new Questao("O conteúdo foi adequeado e é aplicável no seu dia-a-dia?", 3, true, TipoResposta.RESPOSTA_SIMNAO, questionario);
        q4.getAlternativas().add(new Alternativa("Sim", 1, 0, q4));
        q4.getAlternativas().add(new Alternativa("Não", 0, 1, q4));
        questionario.getQuestoes().add(q4);
        
        Questao q5 = new Questao("Marque as opções verdadeiras", 4, false, TipoResposta.RESPOSTA_LISTAMULTIPLA, questionario);
        q5.getAlternativas().add(new Alternativa("Possor aplicar o que aprendi imediatamente no meu dia-a-dia", null, 0, q5));
        q5.getAlternativas().add(new Alternativa("O instrutor dominava o conteúdo apresentado", null, 1, q5));
        q5.getAlternativas().add(new Alternativa("Todos ficaram atentos ao curso durante toda sua duração", null, 2, q5));
        q5.getAlternativas().add(new Alternativa("Poucas pessoas faltaram ou se ausentaram durante o treinamento", null, 3, q5));
        q5.getAlternativas().add(new Alternativa("Gostaria que houvesse outros treinamentos como esse", null, 4, q5));
        q5.getAlternativas().add(new Alternativa("Precisamos de mais aprofundamento no tema apresentado", null, 5, q5));
        questionario.getQuestoes().add(q5);
        
        Questao q6 = new Questao("Deixe seu comentário", 5, false, TipoResposta.RESPOSTA_DISSERTATIVA, questionario);        
        questionario.getQuestoes().add(q6);
        
        return questionario;
    }
    
    
}
