/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.avalieasy.util;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author wellington
 */
public class DbConn {
    
    private static EntityManager entityManager;
    
    private DbConn() {
    }    
    
    public static EntityManager getInstance() {
        if(entityManager==null)
            entityManager = Persistence.createEntityManagerFactory("avalieasyPU").createEntityManager();
        return entityManager;
    }
    
    
}
