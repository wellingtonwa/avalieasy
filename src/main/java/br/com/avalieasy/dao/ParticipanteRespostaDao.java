/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.avalieasy.dao;

import br.com.avalieasy.util.DbConn;
import br.com.avalieasy.model.Resposta_JSON;
import javax.persistence.EntityManager;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class ParticipanteRespostaDao {
    private EntityManager em;

    public ParticipanteRespostaDao() {
        this.em = DbConn.getInstance();
    }
    
    public void salvarRespostaJson(Resposta_JSON resposta) {
        try{
            em.getTransaction().begin();
            em.createNativeQuery("INSERT INTO participante_resposta VALUES(null, ?resposta, ?alternativa, ?participante, ?questao)")
                    .setParameter("resposta", resposta.getResposta())
                    .setParameter("alternativa", resposta.getAlternativa())
                    .setParameter("participante", resposta.getParticipante())
                    .setParameter("questao", resposta.getQuestao())
                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception ex){
            em.getTransaction().rollback();
            throw ex;
        }
    }
    
    
}
