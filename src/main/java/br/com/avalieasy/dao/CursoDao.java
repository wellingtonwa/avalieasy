/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.avalieasy.dao;

import br.com.avalieasy.util.DbConn;
import br.com.avalieasy.model.Curso;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author wellington
 */
public class CursoDao {

    private EntityManager em;

    public CursoDao() {
        this.em = DbConn.getInstance();
    }

    public Curso getCursoByID(Long id) {
        String consulta = "SELECT curso FROM Curso curso WHERE curso.id = :id";
        Query query = em.createQuery(consulta);
        query.setParameter("id", id);
        try {
            return (Curso) query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    public Curso getCursoByNome(String nome) {
        String consulta = "SELECT curso FROM Curso curso WHERE LOWER(curso.nome) like :nome";
        Query query = em.createQuery(consulta);
        query.setParameter("nome", nome.toLowerCase());
        query.setMaxResults(1);
        try {
            return (Curso) query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    public List<Curso> getCursosByNome(String nome) {
        String consulta = "SELECT curso FROM Curso curso WHERE curso.nome = :nome";
        Query query = em.createQuery(consulta);
        query.setParameter("nome", nome);
        return query.getResultList();
    }

    public Curso save(Curso curso) throws Exception {
        try {
            em.getTransaction().begin();
            if (curso.getId() == null) {
                em.persist(curso);
            } else {
                curso = em.merge(curso);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
        return curso;
    }

}
