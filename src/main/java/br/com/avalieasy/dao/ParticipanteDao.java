/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.avalieasy.dao;

import br.com.avalieasy.model.Participante;
import br.com.avalieasy.util.DbConn;
import javax.persistence.EntityManager;

/**
 *
 * @author wellington
 */
public class ParticipanteDao {
    
    private EntityManager em;
    
    public ParticipanteDao(){
        this.em = DbConn.getInstance();
    }
    
    public Participante save(Participante participante) throws Exception {
        try {
            em.getTransaction().begin();
            if (participante.getId() == null) {
                em.persist(participante);
            } else {
                participante = em.merge(participante);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
        return participante;
    }
    
}
