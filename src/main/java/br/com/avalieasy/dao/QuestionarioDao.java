/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.avalieasy.dao;

import br.com.avalieasy.util.DbConn;
import br.com.avalieasy.model.Questao;
import br.com.avalieasy.model.ResSimNao;
import br.com.avalieasy.model.TipoResposta;
import com.sun.org.apache.bcel.internal.classfile.PMGClass;
import java.math.BigDecimal;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author wellington
 */
public class QuestionarioDao {

    private EntityManager em;

    public QuestionarioDao() {
        this.em = DbConn.getInstance();
    }

    public BigDecimal getMediaQuestaoNota(Questao questao) {
        return getMediaQuestaoNota(questao.getId());
    }

    public BigDecimal getMediaQuestaoNota(Long idQuestao) {
        String consulta = "SELECT DISTINCT "
                + " (SELECT SUM(alt.valor)/COUNT(resp.id) FROM ParticipanteResposta resp"
                + " JOIN resp.alternativa alt WHERE resp.questao = questao)"
                + " FROM Curso curso "
                + " JOIN curso.questionario questionario "
                + " JOIN questionario.questoes questao "
                + " JOIN questao.alternativas alternativa "
                + " WHERE questao.id = :idQuestao " 
                + " AND questao.tipoResposta = :tipoResposta"; 
        Query query = em.createQuery(consulta).setParameter("idQuestao", idQuestao)
                .setParameter("tipoResposta", TipoResposta.RESPOSTA_NOTA);
        BigDecimal object = (BigDecimal) query.getSingleResult();
        if(object!=null){
            return object;
        } else {
            return null;
        }
    }

    public Long getPorcentagemQuestaoSim(Long idQuestao) {
        String consulta = "SELECT (COUNT(res.id)*100)/(SELECT COUNT(res1.id) "
                + "						FROM ParticipanteResposta res1 "
                + "                                             JOIN res1.alternativa alt1 "
                + "						WHERE res1.questao = que"
                + "						AND res1.questao = que AND alt1.valor=:resposta)"
                + "FROM ParticipanteResposta res"
                + " JOIN res.questao que WHERE que.id = :idQuestao";
        Query query = em.createQuery(consulta)
                .setParameter("idQuestao", idQuestao)
                .setParameter("resposta", 1);
        return (Long) query.getSingleResult();
    }
    
    public Long getPorcentagemQuestaoNao(Long idQuestao) {
        String consulta = "SELECT (COUNT(res.id)*100)/(SELECT COUNT(res1.id) "
                + "						FROM ParticipanteResposta res1 "
                + "                                             JOIN res1.alternativa alt1 "
                + "						WHERE res1.questao = que"
                + "						AND res1.questao = que AND alt1.valor=:resposta)"
                + "FROM ParticipanteResposta res"
                + " JOIN res.questao que WHERE que.id = :idQuestao";
        Query query = em.createQuery(consulta)
                .setParameter("idQuestao", idQuestao)
                .setParameter("resposta", 0);
        return (Long) query.getSingleResult();
    }
    
    public Long getTotalParticipantesQuestionario(Long idQuestionario) {
        String consulta = "SELECT COUNT(DISTINCT part.id) "
                + "     FROM Questionario quest"
                + "     JOIN quest.questoes ques"
                + "     JOIN ques.respostas res"
                + "     JOIN res.participante part"
                + "     WHERE quest.id = :idQuestionario";
        Query query = em.createQuery(consulta).setParameter("idQuestionario", idQuestionario);
        return (Long) query.getSingleResult();
    }
    

}
