/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.avalieasy.controller;

import br.com.avalieasy.dao.CursoDao;
import br.com.avalieasy.dao.QuestionarioDao;
import br.com.avalieasy.model.Curso;
import br.com.avalieasy.model.Questao;
import br.com.avalieasy.model.ResultadoParcial_JSON;
import br.com.avalieasy.model.TipoResposta;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wellington
 */
public class Parcial extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String idCurso = req.getParameter("idCurso");
        if(idCurso!=null && isNumeric(idCurso)){
            String jsonResultado = new ObjectMapper().writeValueAsString(getResultadosQuestionario(idCurso));
            resp.getWriter().print(jsonResultado);
        }
    }
    
    public List<ResultadoParcial_JSON> getResultadosQuestionario(String idCurso) {
        
        QuestionarioDao questionarioDao = new QuestionarioDao();
        
        Curso curso = new CursoDao().getCursoByID(Long.parseLong(idCurso));
        Long totalParticipantes = questionarioDao
                .getTotalParticipantesQuestionario(curso.getQuestionario().getId());
        List<ResultadoParcial_JSON> listaResultadoParcial = new ArrayList<ResultadoParcial_JSON>();
        for(Questao forQuestao : curso.getQuestionario().getQuestoes()){
            if(forQuestao.getTipoResposta()==TipoResposta.RESPOSTA_NOTA){
                BigDecimal mediaQuestao = questionarioDao.getMediaQuestaoNota(forQuestao);
               listaResultadoParcial.add(new ResultadoParcial_JSON(forQuestao.getEnunciado()
                       , forQuestao.getTipoResposta(), null, String.valueOf(mediaQuestao)));
            }
        }
        return listaResultadoParcial;
    }
    
    
    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
    
}
