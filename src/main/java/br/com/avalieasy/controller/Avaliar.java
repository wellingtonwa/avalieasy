/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.avalieasy.controller;

import br.com.avalieasy.dao.CursoDao;
import br.com.avalieasy.dao.ParticipanteDao;
import br.com.avalieasy.dao.ParticipanteRespostaDao;
import br.com.avalieasy.util.TemplateQuestionario;
import br.com.avalieasy.model.Curso;
import br.com.avalieasy.model.Participante;
import br.com.avalieasy.model.Resposta_JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wellington
 */
public class Avaliar extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String nome = (String) request.getParameter("nome");

        Curso curso = getCurso(nome);

        try {
            String jsonCurso = new ObjectMapper().writeValueAsString(curso);
            response.setStatus(200);
            response.getWriter().print(jsonCurso);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        StringBuffer sb = new StringBuffer();
        
        try {
            BufferedReader reader = request.getReader();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            response.getWriter().print("{\"status\":\"500\", \"mensagem\": \""+e.getMessage()+"\"}");
        }
        
        response.getWriter().print(salvarRespostas(sb.toString(), request.getRemoteAddr()));

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    public String salvarRespostas(String json, String ipParticipante) {
        List<Resposta_JSON> listRespostas = null;
        try {
            listRespostas = new Resposta_JSON().convertJsonList(json);
        } catch (IOException ex) {
            return "{\"status\":\"500\", \"mensagem\":\""+ex.getMessage()+"\"}";
        }
        
        Participante participante = new Participante(ipParticipante);
        try {
            new ParticipanteDao().save(participante);
        } catch (Exception ex) {
            return "{\"status\":\"500\", \"mensagem\":\""+ex.getMessage()+"\"}";
        }
        
        ParticipanteRespostaDao participanteRespostaDao = new ParticipanteRespostaDao();
        for(Resposta_JSON forResposta : listRespostas){
            forResposta.setParticipante(participante.getId());
            participanteRespostaDao.salvarRespostaJson(forResposta);
        }
        return "{\"status\":\"200\", \"mensagem\":\"Dados salvos com sucesso!\"}";
    }
    
    public Curso getCurso(String nomeCurso) {

        if (nomeCurso != null && !nomeCurso.trim().isEmpty()) {
            Curso curso;
            // Verificando se a string passada é um valor númerico
            if (isNumeric(nomeCurso)) {
                curso = new CursoDao().getCursoByID(Long.parseLong(nomeCurso.trim()));
            } else {
                curso = new CursoDao().getCursoByNome(nomeCurso);
            }

            if (curso == null && !isNumeric(nomeCurso)) {
                // Devo cadastrar o curso e adicionar o questionário nele
                curso = new Curso(nomeCurso, TemplateQuestionario.getQuestionario(nomeCurso));
                curso.getQuestionario().setCurso(curso);
                try {
                    new CursoDao().save(curso);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            return curso;
        }
        return null;
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

}
