/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.avalieasy.model;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class ResultadoParcial_JSON {
    
    private String enunciado;
    private TipoResposta tipoResposta;
    private String descricaoAlternativa;
    private String valor;

    public ResultadoParcial_JSON(String enunciado, TipoResposta tipoResposta, String descricaoAlternativa, String valor) {
        this.enunciado = enunciado;
        this.tipoResposta = tipoResposta;
        this.descricaoAlternativa = descricaoAlternativa;
        this.valor = valor;
    }

    
    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public TipoResposta getTipoResposta() {
        return tipoResposta;
    }

    public void setTipoResposta(TipoResposta tipoResposta) {
        this.tipoResposta = tipoResposta;
    }

    public String getDescricaoAlternativa() {
        return descricaoAlternativa;
    }

    public void setDescricaoAlternativa(String descricaoAlternativa) {
        this.descricaoAlternativa = descricaoAlternativa;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
