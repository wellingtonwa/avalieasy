/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.avalieasy.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
@Entity
@Table(name = "alternativa")
public class Alternativa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String descricao;
    private Integer valor;
    @Column(nullable = false)
    private Integer ordem;
    
    
    // Relacionamentos
    @ManyToOne
    @JoinColumn(name = "ID_QUESTAO")
    @JsonBackReference
    private Questao questao;
    @OneToMany(mappedBy = "alternativa")
    @JsonBackReference
    private List<ParticipanteResposta> respostaParticitantes;

    public Alternativa(String descricao, Integer valor, Integer ordem, Questao questao) {
        this.descricao = descricao;
        this.valor = valor;
        this.ordem = ordem;
        this.questao = questao;
    }

    public Alternativa() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public Integer getOrdem() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem = ordem;
    }

    public Questao getQuestao() {
        return questao;
    }

    public void setQuestao(Questao questao) {
        this.questao = questao;
    }

    public List<ParticipanteResposta> getRespostaParticitantes() {
        if(respostaParticitantes==null){
            respostaParticitantes = new ArrayList<>();
        }
        return respostaParticitantes;
    }

    public void setRespostaParticitantes(List<ParticipanteResposta> respostaParticitantes) {
        this.respostaParticitantes = respostaParticitantes;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Alternativa other = (Alternativa) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    
    
}
