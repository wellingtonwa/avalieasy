/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.avalieasy.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class Resposta_JSON {
    
    private Long id;
    private Long participante;
    private Long alternativa;
    private String resposta;
    private Long questao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParticipante() {
        return participante;
    }

    public void setParticipante(Long participante) {
        this.participante = participante;
    }

    public Long getAlternativa() {
        return alternativa;
    }

    public void setAlternativa(Long alternativa) {
        this.alternativa = alternativa;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public Long getQuestao() {
        return questao;
    }

    public void setQuestao(Long questao) {
        this.questao = questao;
    }
    
    public List<Resposta_JSON> convertJsonList(String json) throws IOException {
        return new ObjectMapper().readValue(json, new TypeReference<List<Resposta_JSON>>(){});
    }
    
    public Resposta_JSON convertJson(String json) throws IOException {
        return new ObjectMapper().readValue(json, Resposta_JSON.class);
    }
    
}
