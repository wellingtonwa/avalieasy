/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.avalieasy.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 *
 * @author wellington
 */
@Entity
@Table(name = "questao")
public class Questao implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String enunciado;
    private Integer ordem;
    private Boolean obrigatoria;
    @Column(name = "TIPO_RESPOSTA")
    private TipoResposta tipoResposta;
    @ManyToOne
    @JoinColumn(name = "ID_QUESTIONARIO")
    @JsonBackReference
    private Questionario questionario;
    @OneToMany(mappedBy = "questao", cascade = CascadeType.ALL)
    @OrderBy("ordem")
    private List<Alternativa> alternativas;
    @OneToMany(mappedBy = "questao")
    @JsonBackReference
    private List<ParticipanteResposta> respostas;
    
    public Questao() {
    }

    public Questao(String enunciado, Integer ordem, Boolean obrigatoria, TipoResposta tipoResposta, Questionario questionario) {
        this.enunciado = enunciado;
        this.ordem = ordem;
        this.obrigatoria = obrigatoria;
        this.tipoResposta = tipoResposta;
        this.questionario = questionario;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public Integer getOrdem() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem = ordem;
    }

    public Questionario getQuestionario() {
        return questionario;
    }

    public void setQuestionario(Questionario questionario) {
        this.questionario = questionario;
    }

    public TipoResposta getTipoResposta() {
        return tipoResposta;
    }

    public void setTipoResposta(TipoResposta tipoResposta) {
        this.tipoResposta = tipoResposta;
    }

    public Boolean getObrigatoria() {
        return obrigatoria;
    }

    public void setObrigatoria(Boolean obrigatoria) {
        this.obrigatoria = obrigatoria;
    }

    public List<Alternativa> getAlternativas() {
        if(alternativas==null)
            alternativas = new ArrayList<>();
        return alternativas;
    }

    public void setAlternativas(List<Alternativa> alternativas) {
        this.alternativas = alternativas;
    }

    public List<ParticipanteResposta> getRespostas() {
        if(respostas == null)
            respostas = new ArrayList<>();
        return respostas;
    }

    public void setRespostas(List<ParticipanteResposta> respostas) {
        this.respostas = respostas;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Questao other = (Questao) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    
}
