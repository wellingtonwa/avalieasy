/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.avalieasy.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
@Entity
@Table(name = "participante")
public class Participante implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String ip;
    @OneToMany(mappedBy = "participante")
    private List<ParticipanteResposta> participanteResposta;

    public Participante() {
    }

    public Participante(String ip) {
        this.ip = ip;
    }

    public Participante(String ip, List<ParticipanteResposta> participanteResposta) {
        this.ip = ip;
        this.participanteResposta = participanteResposta;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public List<ParticipanteResposta> getParticipanteResposta() {
        if(participanteResposta==null){
            participanteResposta = new ArrayList<>();
        }
        return participanteResposta;
    }

    public void setParticipanteResposta(List<ParticipanteResposta> participanteResposta) {
        this.participanteResposta = participanteResposta;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Participante other = (Participante) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}
