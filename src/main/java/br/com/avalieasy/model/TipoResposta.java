/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.avalieasy.model;

/**
 *
 * @author wellington
 */
public enum TipoResposta {

    RESPOSTA_DISSERTATIVA("dissertativa"),
    RESPOSTA_LISTAUNICA("lista_unica"),
    RESPOSTA_LISTAMULTIPLA("lista_multipla"),
    RESPOSTA_SIMNAO("sim_nao"),
    RESPOSTA_NOTA("nota");

    private String tipo;

    TipoResposta(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
